#!/bin/sh
PROJECT=$(oc project -q)
REPLICAS=1
APP_NAME=verify-cluster
S2I_TEMPLATE=eap73-openjdk11-basic-s2i
IMAGE_STREAM_NAMESPACE=openshift
SOURCE_REPOSITORY_URL=https://gitlab.com/ocp-demo/verify-cluster.git
SOURCE_REPOSITORY_REF=master
EAP_IMAGE_NAME=jboss-eap73-openjdk11-openshift:7.3
#EAP_RUNTIME_IMAGE_NAME=jboss-eap73-openjdk11-openshift:7.3
#EAP_RUNTIME_IMAGE_NAME=jboss-eap73-openjdk11-runtime-openshift:7.3
GALLEON_PROVISION_LAYERS=web-clustering
#GALLEON_PROVISION_LAYERS=datasources-web-server,web-clustering
CONTEXT_DIR=
oc new-app \
--template=$S2I_TEMPLATE \
--param=APPLICATION_NAME=$APP_NAME \
--param=IMAGE_STREAM_NAMESPACE=$IMAGE_STREAM_NAMESPACE \
--param=SOURCE_REPOSITORY_URL=$SOURCE_REPOSITORY_URL \
--param=SOURCE_REPOSITORY_REF=$SOURCE_REPOSITORY_REF \
--param=EAP_IMAGE_NAME=$EAP_IMAGE_NAME \
--param=CONTEXT_DIR=$CONTEXT_DIR \
--param=GALLEON_PROVISION_LAYERS=$GALLEON_PROVISION_LAYERS \
-n $PROJECT
sleep 60
oc logs -f build/$(oc get build | grep Running | grep ${APP_NAME} |  head -n 1| cut -d ' ' -f 1) -n $PROJECT
sleep 60
oc logs -f build/$(oc get build | grep Running | grep ${APP_NAME} | head -n 1| cut -d ' ' -f 1) -n $PROJECT
oc rollout pause dc $APP_NAME -n $PROJECT
oc scale dc $APP_NAME --replicas=$REPLICAS -n $PROJECT
oc set env dc $APP_NAME JGROUPS_PING_PROTOCOL=kubernetes.KUBE_PING -n $PROJECT
oc set env dc $APP_NAME KUBERNETES_LABELS=application=$APP_NAME -n $PROJECT
oc set env dc $APP_NAME KUBERNETES_NAMESPACE=$PROJECT -n $PROJECT
oc adm policy add-cluster-role-to-user view system:serviceaccount:$PROJECT:default -n $PROJECT
oc rollout resume dc $APP_NAME -n $PROJECT
