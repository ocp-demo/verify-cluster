#!/bin/sh
APP_NAME=verify-cluster
PROJECT=$(oc project -q)
IMAGE_STREAM=jboss-eap73-openshift:7.3
REPLICAS=2
VERSION=1.0.0
BINARY_PATH=deployments/verify-cluster.war
echo "### Create Build Config with local WAR file in deployments directory ###"
echo "### Application Name:     $APP_NAME"
echo "### Image Stream:         $IMAGE_STREAM"
echo "### Deploy to project:    $PROJECT"
echo "### Number of Replicas:   $REPLICAS"
echo "### Version:              $VERSION"
echo "Press any keys to continue"
read
oc new-build --binary=true \
--name=${APP_NAME} -l app=${APP_NAME} --image-stream=${IMAGE_STREAM}
oc start-build ${APP_NAME} \
--from-file=${BINARY_PATH} \
--follow
sleep 3
oc tag ${APP_NAME}:latest ${APP_NAME}:${VERSION}
clear
oc get buildconfig/${APP_NAME}
oc get is/${APP_NAME}
echo "\nPress any keys to continue"
read

echo "### Start create $APP_NAME and configure standalone cluster"
oc new-app ${APP_NAME}:${VERSION} \
--labels=app=${APP_NAME},deploymentconfig=${APP_NAME},version=${VERSION},app.kubernetes.io/name=jboss
echo "### Set service account"
oc adm policy add-cluster-role-to-user view system:serviceaccount:$PROJECT:default -n $PROJECT
echo "### Set JGROUPS"
oc rollout pause deployment  ${APP_NAME}
oc set env deployment ${APP_NAME} \
JGROUPS_PING_PROTOCOL=kubernetes.KUBE_PING \
KUBERNETES_NAMESPACE=${PROJECT} \
KUBERNETES_LABELS=app=${APP_NAME},version=${VERSION}
#oc set env deployment ${APP_NAME} DISABLE_EMBEDDED_JMS_BROKER=true
oc scale deployment ${APP_NAME} --replicas=${REPLICAS}
oc rollout resume deployment  ${APP_NAME}
oc expose svc/${APP_NAME}
watch oc get pods
echo "URL: http://$(oc get route ${APP_NAME} -n ${PROJECT} -o jsonpath='{.spec.host}')/verify-cluster"
