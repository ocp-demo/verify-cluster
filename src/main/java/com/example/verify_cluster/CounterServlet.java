package com.example.verify_cluster;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.util.Date;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import org.jboss.logging.Logger;

@WebServlet({"/counter"})
public class CounterServlet extends HttpServlet  {
    private static final long serialVersionUID = 1L;
    private static Logger LOGGER = Logger.getLogger(CounterServlet.class);
    
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
       
         final HttpSession session = request.getSession(true);
         int visitCount;
         // &&
         if ( session.isNew()) {
           LOGGER.info("New Session");
           visitCount = 1;
         } else {
            if(session.getAttribute("counter")!=null){
               visitCount = ((Integer)session.getAttribute("counter")).intValue();
               ++visitCount;	
               LOGGER.info("Increment visitCount to "+visitCount);
            }else{
               LOGGER.info("Session is invalidated");
               visitCount = 1;
            }
         }
         
         session.setAttribute("counter", visitCount);
         final InetAddress ip = InetAddress.getLocalHost();
         final String hostname = ip.getHostName();
         final Date now = new Date(session.getLastAccessedTime());
         LOGGER.info("Session:"+ session.getId() +", IP :" + ip+", Hostname: " + hostname+", Visit Count:" + visitCount);
         final String title = "Cluster Verification";
         response.setContentType("text/html");
         final PrintWriter out = response.getWriter();
         final String docType = "<!doctype html public \"-//w3c//dtd html 4.0 transitional//en\">";
         final String head = "<head><link rel=\"stylesheet\" type=\"text/css\" href=\"css/table.css\"><title>"+title+"</title></head>";
         out.println(
               docType+"<html lang=\"en\" class>"+head+
               "<body>\n"+
               "<div class=\"conatainer\">\n"+
               "<div class=\"gold\">\n"+
               "<h1>Session ID</h1>\n"+
               "<h2>"+session.getId()+"<h2>\n"+
               "<div class=\"price\">"+visitCount+" times</div>\n"+
               "<p>IP Address</p><span>"+ip+"</span>\n"+
               "<p>Hostname</p><span>"+hostname+"</span>\n"+
               "<p>Last Accessed</p><span>"+now+"</span>\n"+
               "<button><a href=\"counter\" class=\"button\">Increment</a></button>"+ 
               "<button><a href=\"invalidate\" class=\"button\">Invalidate</a></button>"+         
               "</div></body></html>"

         );
        //  out.println(docType + 
        //                  "<html>\n" + 
        //                  "<head><title>" + title + 
        //                  "</title></head>\n" + "<body bgcolor=\"#f0f0f0\">\n" + 
        //                  "<h1 align=\"center\">" + title + "</h1>\n" + 
        //                  "<h2 align=\"center\">Session Infomation</h2>\n" + 
        //                  "<table border=\"1\" align=\"center\">\n" + 
        //                  "<tr bgcolor=\"#949494\">\n" + 
        //                  "  <th>Session info</th><th>value</th></tr>\n" + 
        //                  "<tr>\n" + "  <td>id</td>\n" + "  <td>" + session.getId() + 
        //                  "</td></tr>\n" + "<tr>\n" + "  <td>Hostname: </td>\n" + 
        //                  "  <td>" + hostname + "  </td></tr>\n" + "<tr>\n" + 
        //                  "  <td>IP Address</td>\n" + 
        //                  "  <td>" + ip + "  </td></tr>\n" + "  <td>Number of visits</td>\n" + 
        //                  "  <td>" + visitCount + "</td></tr>\n" + "  <td>Last Visit</td>\n" + 
        //                  "  <td>" + now + "</td></tr>\n" + "</table>\n" + "</body></html>");
      }
      protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
         this.doGet(request, response);
      }
}