package com.example.verify_cluster;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.util.Date;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import org.jboss.logging.Logger;

@WebServlet({"/invalidate"})
public class InvalidateCounterServlet extends HttpServlet  {
    private static final long serialVersionUID = 1L;
    private static Logger LOGGER = Logger.getLogger(InvalidateCounterServlet.class);
    
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
         final String title = "Cluster Verification";
         response.setContentType("text/html");
         final HttpSession session = request.getSession(true);
         session.invalidate();
         LOGGER.info("Invalidate Session");
         final PrintWriter out = response.getWriter();
         final String docType = "<!doctype html public \"-//w3c//dtd html 4.0 transitional//en\">";
         final String head = "<head><link rel=\"stylesheet\" type=\"text/css\" href=\"css/table.css\"><title>"+title+"</title></head>";
         out.println(
               docType+"<html lang=\"en\" class>"+head+
               "<body>\n"+
               "<div class=\"conatainer\">\n"+
               "<div class=\"gold\">\n"+
               "<h1>Session ID</h1>\n"+
               "<h2>"+session.getId()+"<h2>\n"+
               "<div class=\"price\"> times</div>\n"+
               "<p>IP Address</p><span></span>\n"+
               "<p>Hostname</p><span></span>\n"+
               "<p>Last Accessed</p><span></span>\n"+
               "<button><a href=\"invalidate\" class=\"button\">Invalidate</a></button>"+         
               "</div></body></html>");
         response.sendRedirect(request.getContextPath() + "/counter");
    }
      protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
         this.doGet(request, response);
      }
}